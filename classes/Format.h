#ifndef FORMAT_H
#define FORMAT_H
#include <iostream>

class Format
{

 private :
  unsigned int id;
  std::string entitled;

 public :
  Format();
  Format(unsigned int, std::string);
  ~Format();

  void setId(unsigned int);
  void setEntitled(std::string);
  unsigned int getId();
  std::string getEntitled();
};

#endif
