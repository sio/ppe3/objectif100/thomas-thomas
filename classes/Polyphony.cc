#include "Polyphony.h"
#include <iostream>

Polyphony::Polyphony() :
  id(0),
  number(0)
{}

Polyphony::Polyphony(unsigned int _id, unsigned short int _number) :
  id(_id),
  number(_number)
{}

Polyphony::~Polyphony() {}

void Polyphony::setId(unsigned int _id)
{
  id = _id;
}

void Polyphony::setNumber(unsigned short int _number)
{
  number = _number;
}

unsigned int Polyphony::getId()
{
  return id;
}

unsigned short int Polyphony::getNumber()
{
  return number;
}
