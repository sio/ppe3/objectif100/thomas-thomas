
#include "Track.h"
#include <iostream>


Track::Track() :
  id(0),
  duration(0),
  name(""),
  path(""),
  theArtist(),
  theAlbum(),
  thePolyphony(),
  theFormat(),
  theSubgenre(),
  theGenre() {}


Track::Track(unsigned int _id, int _duration, std::string _name, std::string _path, std::vector<Artist> _theArtist, std::vector<Album> _theAlbum, Polyphony _thePolyphony, Format _theFormat, Subgenre _theSubgenre, Genre _theGenre) :
  id(_id),
  duration(_duration),
  name(_name),
  path(_path),
  theArtist(_theArtist),
  anAlbum(_theAlbum),
  thePolyphony(_thePolyphony),
  theFormat(_theFormat),
  theSubgenre(_theSubgenre),
  theGenre(_theGenre) {}

Track::Track(unsigned int _id, int _duration, std::string _name, std::string _path, Artist _theArtist, Album _theAlbum, Polyphony _thePolyphony, Format _theFormat, Subgenre _theSubgenre, Genre _theGenre) :
  id(_id),
  duration(_duration),
  name(_name),
  path(_path),
  theAlbum(_theAlbum),
  thePolyphony(_thePolyphony),
  theFormat(_theFormat),
  theSubgenre(_theSubgenre),
  theGenre(_theGenre),
  anArtist(_theArtist) {}



Track::~Track() {}

void Track:: setId(unsigned int _id)
{
  id = _id;
}
void Track::setDuration(int _duration)
{
  duration = _duration;
}
void Track::setName(std::string _name)
{
  name = _name;
}
void Track::setPath(std::string _path)
{
  path = _path;
}
void Track::setArtist(std::vector<Artist> _theArtist)
{
  theArtist = _theArtist;
}
void Track::setArtist(Artist _theArtist)
{
  anArtist = _theArtist;
}
void Track::setAlbum(Album _theAlbum)
{
  theAlbum = _theAlbum;
}
void Track::setAlbum(std::vector<Album> _theAlbum)
{
  anAlbum = _theAlbum;
}
void Track::setPolyphony(Polyphony _thePolyphony)
{
  thePolyphony = _thePolyphony;
}
void Track::setFormat(Format _theFormat)
{
  theFormat = _theFormat;
}
void Track::setSubgenre(Subgenre _theSubgenre)
{
  theSubgenre = _theSubgenre;
}
void Track::setGenre(Genre _theGenre)
{
  theGenre = _theGenre;
}



unsigned int Track:: getId()
{
  return id;
}
int Track::getDuration()
{
  return duration;
}
std::string Track::getPath()
{
  return path;
}
std::string Track::getName()
{
  return name;
}
std::vector<Artist> Track::getArtist()
{
  return theArtist;
}
Album Track::getAlbum()
{
  return theAlbum;
}
Genre Track::getGenre()
{
  return theGenre;
}
Subgenre Track::getSubgenre()
{
  return theSubgenre;
}
Polyphony Track::getPolyphony()
{
  return thePolyphony;
}
Format Track::getFormat()
{
  return theFormat;
}
Artist Track::getAnArtist()
{
  return anArtist;
}
std::vector<Album> Track::getAnAlbum()
{
  return anAlbum;
}

std::vector<Track> Track::addTrack(std::string _name, PGconn *conn)
{
  std::vector<Track> theTrack;
  std::string requete = "select * from \"radio_libre\".\"morceau\" INNER JOIN \"radio_libre\".\"artiste_morceau\" ON \"morceau\".id = \"artiste_morceau\".id_morceau INNER JOIN \"radio_libre\".\"album_morceau\" ON \"morceau\".id = \"album_morceau\".id_morceau INNER JOIN \"radio_libre\".\"artiste\" ON \"artiste_morceau\".id_artiste = \"artiste\".id INNER JOIN \"radio_libre\".\"album\" ON \"album_morceau\".id_album = \"album\".id INNER JOIN \"radio_libre\".\"polyphonie\" ON \"morceau\".fk_polyphonie=\"polyphonie\".id INNER JOIN \"radio_libre\".\"genre\" ON \"morceau\".fk_genre=\"genre\".id INNER JOIN \"radio_libre\".\"format\" ON morceau.fk_format=format.id where \"morceau\".nom ~ $1;";
  unsigned int howMuchAnswerLine = 0;
  Track newTrack;
  Artist newArtist;
  Album newAlbum;
  Genre newGenre;
  Format newFormat;
  Polyphony newPolyphony;

  
  
  const Oid types[] = {25};
  const char *val[] = {_name.c_str()};
  
  PGresult *resultat =  PQexecParams(conn,
                                     requete.c_str(),
                                     1,
                                     types,
                                     val,
                                     NULL,
                                     NULL,
                                     0);

  howMuchAnswerLine = PQntuples(resultat);

  for(int i = 0; i < howMuchAnswerLine; ++i){
    newArtist = Artist(atoi(PQgetvalue(resultat, i, 12)), std::string(PQgetvalue(resultat, i, 13)));
    newAlbum = Album(atoi(PQgetvalue(resultat, i, 14)), std::string(PQgetvalue(resultat, i, 16)));
    newPolyphony = Polyphony(atoi(PQgetvalue(resultat, i, 17)), atoi(PQgetvalue(resultat, i, 18)));
    newGenre = Genre(atoi(PQgetvalue(resultat, i, 19)), std::string(PQgetvalue(resultat, i, 20)));
    newFormat = Format(atoi(PQgetvalue(resultat, i, 21)),std::string(PQgetvalue(resultat, i, 22)));
Subgenre sub = Subgenre();

    newTrack = Track(atoi(PQgetvalue(resultat, i, 0)), atoi(PQgetvalue(resultat, i, 1)), std::string(PQgetvalue(resultat, i, 2)), std::string(PQgetvalue(resultat, i, 3)), newArtist, newAlbum, newPolyphony, newFormat, sub, newGenre);
    theTrack.push_back(newTrack);
  }
  return theTrack;
} 
