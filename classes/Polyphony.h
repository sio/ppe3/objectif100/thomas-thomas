#ifndef POLYPHONY_H
#define POLYPHONY_H
#include <iostream>

class Polyphony
{

  //attributes

 private :
  unsigned int id;
  unsigned short int number;

  //Constructors

 public :

  Polyphony();
  Polyphony(unsigned int, unsigned short int);
  ~Polyphony();

  //methods

  void setId(unsigned int);
  void setNumber(unsigned short int);
  unsigned short int getNumber();
  unsigned int getId();


};

#endif
