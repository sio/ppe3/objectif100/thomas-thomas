#include <iostream>
#include "Cli.h"

Cli::Cli() :
  id(0),
  genre(std::map<std::string, float>()),
  subgenre(std::map<std::string, float>()) {}

Cli::Cli(unsigned int _id, std::map<std::string, float> _genre, std::map<std::string, float> _subgenre) :
  id(_id),
  genre(_genre),
  subgenre(_subgenre) {}

Cli::~Cli() {}

std::map<std::string, float> Cli::getGenre()
{
  return genre;
}

std::map<std::string, float> Cli::getSubgenre()
{
  return subgenre;
}

unsigned int Cli::getId()
{
  return id;
}

void Cli::setId(unsigned int _id)
{
  id = _id;
}

void Cli::setGenre(std::map<std::string, float> _genre)
{
  genre = _genre;
}

void Cli::setSubgenre(std::map<std::string, float> _subgenre)
{
  subgenre = _subgenre;
}


