#ifndef ALBUM_H
#define ALBUM_H
#include <iostream>
#include <chrono>

class Album
{

  //attributes

 private :
  unsigned int id;
//  std::chrono::system_clock date;
  std::string name;


  //methods
 public:

  Album();
  Album(unsigned id, std::string);
  ~Album();

  //void setDate(std::chrono::system_clock);
  void setId(unsigned int);
  void setName(std::string);

  unsigned int getId();
  //std::chrono::system_clock getDate();
  std::string getName();




};
#endif
